'use strict';

var MenuSysComponent = require('./syscomponents/menu-syscomponent');

exports.start = function() {

	console.log('sysmain start', sys);
	
	var menuSysComponent = new MenuSysComponent({
		menu: {
			type: 'menubar'
		}
	});

	menuSysComponent.render();
}