'use strict';

/**
 * 	Don'ts
 *
 *	In summary, please DO NOT do following things:
 *
 *	Do not recreate UI elements, reuse them.
 *	Do not reassign an element, such as menu.items[0] = item or item = new gui.MenuItem({}).
 *	Do not delete an element, such delete item.
 *	Do not change UI types' prototype.
 */

function MenuSysComponent(options) {

	this.toString = function() {

		return 'MenuSysComponent';
	}

	this.initialize(options);
}

MenuSysComponent.prototype.initialize = function(options) {

	this.options = options;

	this.menu = new gui.Menu(options.menu);
}

MenuSysComponent.prototype.onSaveClick = function() {

	console.log(this + '.onSaveClick()');
}

MenuSysComponent.prototype.fileMenu = function() {

	var menu = new gui.MenuItem({ label: i18n.menu.file });
	var submenu = new gui.Menu();
	
	submenu.append(new gui.MenuItem({ 
		label: i18n.menu.save + ' ' + '('+ i18n.menu.controlS +')',
		key: 's',
		modifiers: 'ctrl',
		click: this.onSaveClick.bind(this)
	}));

	menu.submenu = submenu;

	return menu;
}

MenuSysComponent.prototype.viewMenu = function() {

	var menu = new gui.MenuItem({ label: i18n.menu.view });
	var submenu;

	return menu;
}

MenuSysComponent.prototype.helpMenu = function() {

	var menu = new gui.MenuItem({ label: i18n.menu.help });
	var submenu;

	return menu;
}

MenuSysComponent.prototype.buildMenu = function() {
	
	// File Menu
	this.menu.append(this.fileMenu());

	// View Menu
	this.menu.append(this.viewMenu());
	
	// Help Menu
	this.menu.append(this.helpMenu());
}

MenuSysComponent.prototype.render = function() {

	this.buildMenu();
	
	gui.Window.get().menu = this.menu;

	return this;
}

module.exports = MenuSysComponent;