import BaseProxy from 'js/libs/base-proxy';

import ProjectsCollection from 'js/collections/projects-collection';

class ProjectsProxy extends BaseProxy {
	
	constructor(options) {

		// super
		super(options);

		this.collection = new ProjectsCollection();

		this.collection.on('reset', this.loaded.bind(this))
	}

	loadAll() {

		this.collection.fetch({
			reset: true
		});
	}

	loaded() {

		this.eventEmitter.emit(AppConsts.PROJECTS_LOADED, this.collection);
	}
}

ProjectsProxy.prototype.NAME = 'ProjectsProxy';

export default ProjectsProxy;