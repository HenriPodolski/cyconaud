import BaseProxy from 'js/libs/base-proxy';

import ContentsCollection from 'js/collections/contents-collection';

class ContentsProxy extends BaseProxy {
	
	constructor(options) {

		// super
		super(options);

		this.collection = new ContentsCollection();

		this.collection.on('reset', this.loaded.bind(this))
	}

	loadAll() {

		this.collection.fetch({
			reset: true
		});
	}

	loaded() {

		this.eventEmitter.emit(AppConsts.CONTENTS_LOADED, this.collection);
	}
}

ContentsProxy.prototype.NAME = 'ContentsProxy';

export default ContentsProxy;