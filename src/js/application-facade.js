import AppConsts from 'shared/app-consts';
import UILocales from 'shared/locales';
import StartupCommand from 'js/commands/startup-command';
import ContentTypeAddCommand from 'js/commands/contenttype-add-command';
import WorkspaceReadyCommand from 'js/commands/workspace-ready-command';
import EventEmitter from 'js/libs/event-emitter';

window.i18n = {};
window.AppConsts = AppConsts;

let singleton = Symbol();
let singletonEnforcer = Symbol();

class ApplicationFacade {

	constructor(enforcer) {
		
		if(enforcer != singletonEnforcer) {

			throw "Cannot construct singleton";
		}

		this.eventEmitter = EventEmitter.instance;
		
		this.lang = 'en'

		i18n = UILocales[this.lang]; 

		this.proxies = {};

		this.mediators = {};

		this.initializeController();
	}

	static get instance() {
		
		if(!this[singleton]) {
		
			this[singleton] = new ApplicationFacade(singletonEnforcer);
		}
		
		return this[singleton];
	}

	initializeController() {

		this.registerCommand(AppConsts.STARTUP, StartupCommand);

		this.registerCommand(AppConsts.CONTENT_TYPE_ADD, ContentTypeAddCommand);
		
		this.registerCommand(AppConsts.WORKSPACE_READY, WorkspaceReadyCommand);
	}

	startup() {

		this.sendNotification(AppConsts.STARTUP);
	}

	registerCommand(evtString, command) {

		this.eventEmitter.on(evtString, function(note){
			
			var commandInstance = new command();
			commandInstance.execute.apply(commandInstance, arguments);
		});
	}

	registerProxy(proxy) {

		this.proxies[proxy.NAME] = proxy
		
		return this.proxies[proxy.NAME];
	}

	retrieveProxy(name) {

		return this.proxies[name].NAME && this.proxies[name];
	}

	registerMediator(mediator) {

		this.mediators[mediator.NAME] = mediator
		
		return this.mediators[mediator.NAME];
	}

	retrieveMediator(name) {

		return this.mediators[name].NAME && this.mediators[name];
	}

	sendNotification(evtString, note) {

		this.eventEmitter.emit(evtString, note);
	}
}

window.facade = ApplicationFacade.instance;
facade.startup();

export default {};