import BaseCommand from 'js/libs/base-command';
import ApplicationMediator from 'js/mediators/application-mediator';
import Application from 'js/views/applications/application.jsx!';
// import SidebarWebComponent from 'js/webcomponents/sidebar-webcomponent';
// import WorkspaceWebComponent from 'js/webcomponents/workspace-webcomponent';

class ViewsStartupCommand extends BaseCommand {
	
	execute(note) {

		// new SidebarWebComponent();
		// new WorkspaceWebComponent();	
		
		facade.registerMediator(new ApplicationMediator(Application));	
	}
}

export default ViewsStartupCommand;