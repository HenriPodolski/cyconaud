import BaseCommand from 'js/libs/base-command';

import ProxiesStartupCommand from 'js/commands/proxies-startup-command';
import ViewsStartupCommand from 'js/commands/views-startup-command';

class StartupCommand extends BaseCommand {
	
	execute(note) {

		this.subCommand(ProxiesStartupCommand, note);
		this.subCommand(ViewsStartupCommand, note);
	}
}

export default StartupCommand;