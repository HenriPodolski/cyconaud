import BaseCommand from 'js/libs/base-command';
import ContentFieldMediator from 'js/mediators/content-field-mediator';
import ContentFieldView from 'js/views/content-field-view';

class ContentTypeAddCommand extends BaseCommand {
	
	execute(note) {

		switch(note.name) {

			case(AppConsts.CONTENT_FIELD_ADD): {

				facade.registerMediator(new ContentFieldMediator(
					new ContentFieldView()
				));

				facade.sendNotification('TEST', {name: 'yo'})
				break;
			}

			case(AppConsts.CONTENT_GROUP_ADD): {

				console.log(AppConsts.CONTENT_GROUP_ADD);
				break;
			}

			case(AppConsts.CONTENT_LIST_ADD): {

				console.log(AppConsts.CONTENT_LIST_ADD);
				break;
			}
		}
	}
}

export default ContentTypeAddCommand;