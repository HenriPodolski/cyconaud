import BaseCommand from 'js/libs/base-command';

import ProjectsProxy from 'js/proxies/projects-proxy';
import ContentsProxy from 'js/proxies/contents-proxy';

class ProxiesStartupCommand extends BaseCommand {
	
	execute(note) {

		var projectsProxy = facade.registerProxy(new ProjectsProxy());
		var contentsProxy = new ContentsProxy();

		projectsProxy.loadAll();
		contentsProxy.loadAll();
	}
}

export default ProxiesStartupCommand;