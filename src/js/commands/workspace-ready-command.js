import BaseCommand from 'js/libs/base-command';
import TabsWebComponent from 'js/webcomponents/tabs-webcomponent';

class WorkspaceReadyCommand extends BaseCommand {
	
	execute(note) {

		new TabsWebComponent();
	}
}

export default WorkspaceReadyCommand;