import EventEmitter from 'js/libs/event-emitter';

class BaseCommand {
	
	constructor() {
		
		// this.eventEmitter = EventEmitter.instance;
	}

	execute() {
		// must be implemented
		throw new Error('This must be implemented in the child class');
	}

	subCommand(cmd, note) {
		
		var cmdInstance = Object.create(cmd.prototype);
		cmdInstance.execute.call(cmdInstance, note);
	}

	toString() {

		return this.NAME;
	}
}

BaseCommand.prototype.NAME = 'BaseCommand';

export default BaseCommand;