class BaseWebComponent {
    
	constructor(options) {

    	var self = this;

		var setup = options || {};
		
		var tagName = setup.tagName;

		var View = setup.View;

		var proto = Object.create(setup.proto || HTMLElement.prototype);

		proto.createdCallback = function() {
			
			this.view = new View({el: this});			
		};

		proto.attachedCallback = function() {
			
			this.view.render.call(this.view);
		};

		proto.detachedCallback = function() {
			
			this.view.undelegateEvents.call(this.view);
		};

		proto.attributeChangedCallback = function(name, previousValue, value) {

			if(this.view && typeof this.view.attributeChanged === 'function') {

				this.view.attributeChanged.apply(this.view, arguments);	
			}
		};

		var componentConfig = {
			prototype: proto
		};

		if(setup.extend) {

			componentConfig.extends = setup.extends;
		}

		document.registerElement(tagName, componentConfig);
	}
}

export default BaseWebComponent;