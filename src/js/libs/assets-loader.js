import EventEmitter from 'js/libs/event-emitter';
import $ from 'jquery';

function error(message, ...args){
	
	console.error.apply(console, [message].concat(args));
	console.trace();
}

let singleton = Symbol();
let singletonEnforcer = Symbol();
 
class AssetsLoader {
  
	constructor(enforcer){
		
		if(enforcer != singletonEnforcer) {

			throw 'Cannot construct singleton';
		}

		this.eventEmitter = EventEmitter.instance;

		this.queue = [];

		this.mimetypes = {
			'js': 'text/javascript',
			'css': 'text/css',
			'html': 'text/html',
			'json': 'text/json',
			'default': 'text/plain'
		}
	}

	load(options) {

		var uri = options.uri;
		var id = options.id || 'resource-' + ~~(Math.random() * 1000000000);
		var callback = options.callback
		var scope = options.scope;

		this.queue.push({
			uri: uri,
			id: id, 
			callback: callback, 
			scope: scope
		});
		
		if (this.queue.length == 1) {
			
			this.next();
		}
	}

	request(options) {

		$.ajax(options.uri, {

			success: this.response.bind(this, options),
			error: this.error.bind(this, options)
		});		
	}

	error() {

		console.error('load error', arguments);
	}

	response(options, resource, status, xhr) {
		
		var requestArgs = this.queue.shift();
		var uri = options.uri;
		var id = options.id;
		var callback = options.callback || this.broadcast;
		var scope = options.scope || this;

		callback.call(scope, resource, uri);

		this.next();
	}

	broadcast(resource, uri) {

		// console.log('broadcast', uri);
		this.eventEmitter.emit(uri + ':loaded', resource);
	}

	next() {
		
		if(this.queue.length) {

			var nextArgs = this.queue[0];
			this.request.call(this, nextArgs);
		}
	}

	static get instance() {
		
		if(!this[singleton]) {
		
			this[singleton] = new AssetsLoader(singletonEnforcer);
		}
		
		return this[singleton];
	}
}

export default AssetsLoader;