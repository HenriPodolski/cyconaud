import EventEmitter from 'js/libs/event-emitter';

class BaseProxy {
	
	constructor(options) {

		this.eventEmitter = EventEmitter.instance;

		this.sendNotification = facade.sendNotification;
		// this.registerCommand = facade.registerCommand;
	}

	toString() {

		return this.NAME;
	}
}

BaseProxy.prototype.NAME = 'BaseProxy';

export default BaseProxy;