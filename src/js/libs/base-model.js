import EventEmitter from 'js/libs/event-emitter';
import Backbone from 'backbone';

class BaseModel extends Backbone.Model {
	
	constructor(options) {

		this.eventEmitter = EventEmitter.instance;
		this.sendNotification = facade.sendNotification;
		// this.registerCommand = facade.registerCommand;

		Backbone.Model.prototype.constructor(options);
	}

	toString() {

		return this.NAME;
	}
}

BaseModel.prototype.NAME = 'BaseModel';

export default BaseModel;