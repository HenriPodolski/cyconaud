import EventEmitter from 'js/libs/event-emitter';
import React from 'react/addons';

class BaseMediator {
	
	constructor(viewComponent) {

		this.eventEmitter = EventEmitter.instance;

		this.sendNotification = facade.sendNotification;
		
		this.setViewComponent(viewComponent);

		this.mapNotifications();

		this.onRegister();	
	}

	registerNotification(evtString, callback, note) {

		this.eventEmitter.on(evtString, callback.bind(this), note);
	}

	mapNotifications() {

		var notifiationInterests = this.listNotificationInterests();
		var i = 0;

		for(i; i < notifiationInterests.length; i++) {

			this.registerNotification(notifiationInterests[i], this.handleNotification);
		}
	}	

	setViewComponent(viewComponent) {

		this.viewComponent = viewComponent;
	}

	getViewComponent() {

		return this.viewComponent;
	}

	onRegister() {

		
	}

	onRemove() {

	}

	listNotificationInterests() {
		
		return [];
	}

	handleNotification(note) {
		
	}

	toString() {

		return this.NAME;
	}
}

BaseMediator.prototype.NAME = 'BaseMediator';

export default BaseMediator;