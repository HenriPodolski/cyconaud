import EventEmitter from 'js/libs/event-emitter';
import Backbone from 'backbone';

class BaseCollection extends Backbone.Collection {
	
	constructor(options) {

		this.eventEmitter = EventEmitter.instance;
		this.sendNotification = facade.sendNotification;
		// this.registerCommand = facade.registerCommand;

		Backbone.Collection.prototype.constructor(options);
	}

	toString() {

		return this.NAME;
	}
}

BaseCollection.prototype.NAME = 'BaseCollection';

export default BaseCollection;