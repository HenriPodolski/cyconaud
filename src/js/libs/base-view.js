import EventEmitter from 'js/libs/event-emitter';
import AssetsLoader from 'js/libs/assets-loader';
import $ from 'jquery';
import Backbone from 'backbone';
import _ from 'underscore';

Backbone.$ = $;

class BaseView extends Backbone.View {
	
	constructor(options) {

		this.eventEmitter = EventEmitter.instance;
		this.sendNotification = facade.sendNotification;
		// this.registerCommand = facade.registerCommand;

		Backbone.View.prototype.constructor.call(this, options);
	}

	initialize(options) {

		if(this.templateUrl) {
			
			this.eventEmitter.on(this.templateUrl + ':loaded', this.templateLoaded.bind(this));
			AssetsLoader.instance.load({
				uri: this.templateUrl
			});
		}
	}

	templateLoaded(tpl) {
		
		this.template = _.template(tpl);
		this.includeTemplate();
	}

	includeTemplate() {

		this.$el.html(this.template());
	}

	attributeChanged(name, previousValue, value) {
		
	}

	toString() {

		return this.NAME;
	}
}

BaseView.prototype.NAME = 'BaseView';

export default BaseView;