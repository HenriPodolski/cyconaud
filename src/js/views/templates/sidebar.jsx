/** @jsx React.DOM **/

import React from 'react/addons';
import DragArea from 'js/views/atoms/dragarea.jsx!';

class SidebarView {

	handleClick() {

		console.log('click');
	}

	getInitialState() {

		return {

			width: 0,

			startWidth: 0
		}
	}
	
	componentDidMount() {

		let node = this.getDOMNode();
		let width = node.offsetWidth;

		this.setState({startWidth: width});
	}
	
	handlePositionChange(newPosition, handleSize) {

		let newLeft = newPosition.left + handleSize;
		let width = this.state.startWidth + (newLeft * -1);

		console.log(this.state.startWidth, newLeft, width);

		this.setState({width: width});
	}
	
	render() {

		let sidebarStyle = {
			width: this.state.width + 'px'
		};
		
		return (
            <div id="ui-sidebar" style={sidebarStyle} role="navigation">
                <DragArea position={this.state.position} onPositionChange={this.handlePositionChange} />
            </div>
        );
	}
}

SidebarView.prototype.NAME = 'Sidebar';

var Sidebar = React.createClass(SidebarView.prototype);

export default Sidebar;