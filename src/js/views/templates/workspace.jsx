/** @jsx React.DOM **/

import React from 'react/addons';

class WorkspaceView {
	
	render() {
		
		return (
            <div id="ui-workspace">
                
            </div>
        );
	}
}

WorkspaceView.prototype.NAME = 'Workspace';

var Workspace = React.createClass(WorkspaceView.prototype);

export default Workspace;