import BaseView from 'js/libs/base-view';

class ContentFieldView extends BaseView {
	
	initialize(options) {

		// super
		BaseView.prototype.initialize.call(this, options);		
	}

	render() {
		console.log(this + '.render()');
		return this;
	}
}

ContentFieldView.prototype.NAME = 'ContentFieldView';

export default ContentFieldView;