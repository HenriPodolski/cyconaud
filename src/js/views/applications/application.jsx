/** @jsx React.DOM **/

import React from 'react/addons';
import Workspace from 'js/views/templates/workspace.jsx!';
import Sidebar from 'js/views/templates/sidebar.jsx!';
import Statusbar from 'js/views/organisms/statusbar.jsx!';

class ApplicationView {
	
	render() {
		
		return (
			<div id="ui-application" role="application">
				<div id="ui-main" role="main">
					<Workspace />
					<Sidebar />
				</div>
				<Statusbar />
			</div>
		);
	}
}

ApplicationView.prototype.NAME = 'Application';

var Application = React.createClass(ApplicationView.prototype);

React.render(<Application />, document.body);

export default Application;