/** @jsx React.DOM **/

import React from 'react/addons';

class StatusbarView {
	
	render() {
		
		return (
            <div id="ui-statusbar">
                
            </div>
        );
	}
}

StatusbarView.prototype.NAME = 'Statusbar';

var Statusbar = React.createClass(StatusbarView.prototype);

export default Statusbar;