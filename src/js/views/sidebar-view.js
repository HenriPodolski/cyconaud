import BaseView from 'js/libs/base-view';

class SidebarView extends BaseView {
	
	initialize(options) {

		this.templateUrl = 'tpl/sidebar.tpl.html';

		this.events = {
			'dblclick': function() {
				// some motivation
				$('.motivation')[$('.motivation').css('display') === 'none' ? 'show' : 'hide']();
			}
		}

		// super
		BaseView.prototype.initialize.call(this, options);		
		this.eventEmitter.on(AppConsts.PROJECTS_LOADED, this.onProjectsLoaded.bind(this));
	}

	onProjectsLoaded(projectsCollection) {

		// console.log('onProjectsLoaded', projectsCollection);
	}

	templateLoaded(tpl) {
		
		// super
		BaseView.prototype.templateLoaded.call(this, tpl);
	}

	includeTemplate() {

		BaseView.prototype.includeTemplate.apply(this, arguments);
		this.sendNotification(AppConsts.SIDEBAR_READY);
	}

	render() {

		return this;
	}
}

SidebarView.prototype.NAME = 'SidebarView';

export default SidebarView;