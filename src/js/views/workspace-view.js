import BaseView from 'js/libs/base-view';
import AssetsLoader from 'js/libs/assets-loader';

class WorkspaceView extends BaseView {
	
	initialize(options) {

		// console.log(this.el);

		this.templateUrl = 'tpl/workspace.tpl.html';

		// super
		BaseView.prototype.initialize.call(this, options);

		this.eventEmitter.on(AppConsts.PROJECTS_LOADED, this.onProjectsLoaded.bind(this));
	}

	onProjectsLoaded(projectsCollection) {

		// console.log('onProjectsLoaded', projectsCollection);
	}

	templateLoaded(tpl) {
		
		// super
		BaseView.prototype.templateLoaded.apply(this, arguments);
	}

	includeTemplate() {

		// super
		BaseView.prototype.includeTemplate.apply(this, arguments);
		this.sendNotification(AppConsts.WORKSPACE_READY);
	}

	render() {

		return this;
	}
}

WorkspaceView.prototype.NAME = 'WorkspaceView';

export default WorkspaceView;