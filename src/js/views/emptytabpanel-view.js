import BaseView from 'js/libs/base-view';

class EmptyTabpanelView extends BaseView {
	
	initialize(options) {

		// console.log(this.el);
		// 
		
		this.events = {
			'click .js-add .js-add-field-button': 'onAddFieldBtnClick',
			'click .js-add .js-add-group-button': 'onAddGroupBtnClick',
			'click .js-add .js-add-list-button':  'onAddListBtnClick', 
		}

		this.templateUrl = 'tpl/emptytabpanel.tpl.html';

		// super
		BaseView.prototype.initialize.call(this, options);		
	}

	onAddFieldBtnClick() {

		this.sendNotification(AppConsts.CONTENT_TYPE_ADD, {
			name: AppConsts.CONTENT_FIELD_ADD,
			body: {
				scope: 'main'	
			}			
		});
	}

	onAddGroupBtnClick() {

		this.sendNotification(AppConsts.CONTENT_TYPE_ADD, {
			name: AppConsts.CONTENT_GROUP_ADD,
			body: {
				scope: 'main'	
			}
		});
	}

	onAddListBtnClick() {

		this.sendNotification(AppConsts.CONTENT_TYPE_ADD, {
			name: AppConsts.CONTENT_LIST_ADD,
			body: {
				scope: 'main'	
			}
		});
	}

	templateLoaded(tpl) {
		
		BaseView.prototype.templateLoaded.call(this, tpl);
		
		var rendered = $(this.template(this.data));

		this.$el.replaceWith(rendered);
		this.setElement(rendered);
	}

	render() {

		return this;
	}
}

EmptyTabpanelView.prototype.NAME = 'EmptyTabpanelView';

export default EmptyTabpanelView;