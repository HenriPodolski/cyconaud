/** @jsx React.DOM **/

import React from 'react/addons';
import Draggable from 'react-draggable';

class DragAreaView {

	getInitialState() {

		return {

			handleSize: 0,

			left: null
		}
	}

	componentDidMount() {

		let node = this.getDOMNode();
		let width = node.offsetWidth;
		let left = node.style.left;

		this.setState({
			handleSize: width,
			left: left
		});
	}

	handleStart(evt, ui) {

		this.props.onPositionChange(ui.position, this.state.handleSize);
	}

	handleDrag(evt, ui) {

		this.props.onPositionChange(ui.position, this.state.handleSize);
	}

	handleStop(evt, ui) {

		this.props.onPositionChange(ui.position, this.state.handleSize);
	}

	render() {
		
		var dragHandleStyle = {
			// backgroundColor: 'red'
		}

		return (

		    <Draggable
                axis="x"
                handle=".ui-dragarea"
                start={{x: this.state.left, y: 0}}
                zIndex={100}
                onStart={this.handleStart}
                onDrag={this.handleDrag}
                onStop={this.handleStop}>
            	<div className="ui-dragarea" style={dragHandleStyle} />
            </Draggable>
        );
	}
}

DragAreaView.prototype.NAME = 'DragArea';

var DragArea = React.createClass(DragAreaView.prototype);

export default DragArea;