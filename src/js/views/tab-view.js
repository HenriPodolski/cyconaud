import BaseView from 'js/libs/base-view';
import _ from 'underscore';

class TabView extends BaseView {
	
	initialize(options) {

		// console.log(this.el);
		this.data = options.data;	
		this.templateUrl = 'tpl/tab.tpl.html';

		// super
		BaseView.prototype.initialize.call(this, options);
	}

	templateLoaded(tpl) {
		
		this.template = _.template(tpl);
		this.includeTemplate();
	}

	includeTemplate() {

		var rendered = $(this.template(this.data));

		this.$el.replaceWith(rendered);
		this.setElement(rendered);
	}

	render() {

		return this;
	}
}

TabView.prototype.NAME = 'TabView';

export default TabView;