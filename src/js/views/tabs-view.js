import BaseView from 'js/libs/base-view';
import EmptyTabpanelView from 'js/views/emptytabpanel-view';
import TabView from 'js/views/tab-view';

class TabsView extends BaseView {
	
	initialize(options) {

		// console.log(this.el);

		this.tabsWrapper = this.$('.js-ui-tabswrapper');
		this.tabPanelWrapper = this.$('.js-ui-tabpanelwrapper');

		this.eventEmitter.on(AppConsts.CONTENTS_LOADED, this.onContentsLoaded.bind(this));	
	}

	onContentsLoaded(collection) {

		this.collection = collection;
		this.selectOpenTabsData();
		this.render();
	}

	selectOpenTabsData() {

		if(this.collection) {

			this.openTabsData = this.collection.filter(function(content){

				return content.get('open') === true;
			});

			if(this.openTabsData && this.openTabsData.length > 0) {

				this.removeEmpty();
				this.addAll();
			}
		}		
	}

	removeEmpty() {

		if(this.emptyTabpanel) {

			this.emptyTabpanel.remove();
			this.emptyTabpanel = null;
		}
	}

	addEmpty() {

		// console.log('addEmpty');

		if(!this.hasEmpty) {

			this.emptyTabpanel = new EmptyTabpanelView({
				className: 'js-ui-tabpanel'
			});

			this.emptyTab = new TabView({
				
				data: {
					title: (i18n.new + ' ' + i18n.content),
					isActive: true,
					isNew: true
				}
			});
			
			this.tabPanelWrapper.html(this.emptyTabpanel.render().el);
			this.tabsWrapper.append(this.emptyTab.render().el);

			this.hasEmpty = true;
		}
	}

	addOne(openTabContent) {

		console.log('addOne()', openTabContent.get('type'));
		// new TabpanelView({
		// 	el: 
		// });
	}

	addAll() {

		this.openTabsData.forEach(this.addOne.bind(this));
	}

	render() {

		if(this.openTabsData && this.openTabsData.length > 0) {

			this.addAll();
		} else if(!this.hasEmpty) {

			this.addEmpty();
		}		

		return this;
	}
}

TabsView.prototype.NAME = 'TabsView';

export default TabsView;