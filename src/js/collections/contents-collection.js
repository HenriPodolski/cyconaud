import BaseCollection from 'js/libs/base-collection';
import ContentModel from 'js/models/content-model';
import _ from 'underscore';
import PouchDB from 'pouchdb';
import BackbonePouch from 'js/vendor/backbone-pouch';
import AppConsts from 'shared/app-consts';

class ContentsCollection extends BaseCollection {

	parse(result) {
		
		return _.pluck(result.rows, 'doc');
	}
}

ContentsCollection.prototype.model = ContentModel;

ContentsCollection.prototype.sync = BackbonePouch.sync({
	
	db: PouchDB(AppConsts.DATABASE),
	fetch: 'query',
	options: {
		query: {
			include_docs: true,
			fun: {
				map: function(doc) {
					
					if(doc.type === 'content') {
						
						emit(doc, null)
					}
				}
			}
		}
	}
});

export default ContentsCollection;