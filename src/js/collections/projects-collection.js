import BaseCollection from 'js/libs/base-collection';
import ProjectModel from 'js/models/project-model';
import _ from 'underscore';
import PouchDB from 'pouchdb';
import BackbonePouch from 'js/vendor/backbone-pouch';
import AppConsts from 'shared/app-consts';

class ProjectsCollection extends BaseCollection {

	parse(result) {
		
		return _.pluck(result.rows, 'doc');
	}
}

ProjectsCollection.prototype.model = ProjectModel;

ProjectsCollection.prototype.sync = BackbonePouch.sync({
	
	db: PouchDB(AppConsts.DATABASE),
	fetch: 'query',
	options: {
		query: {
			include_docs: true,
			fun: {
				map: function(doc) {
					
					if(doc.type === 'project') {
						
						emit(doc, null)
					}
				}
			}
		}
	}
});

// // test code
// var pColl = new ProjectsCollection({
// 	test: 'yo',
// 	type: 'project'
// });

// pColl.at(0).save();
// console.log(pColl.at(0).get('test'));


// pColl.fetch({
// 	reset: true
// });

// setTimeout(function(){
// 	console.log(pColl.at(0).toJSON());
// }, 3000);

export default ProjectsCollection;