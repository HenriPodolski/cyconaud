import BaseMediator from 'js/libs/base-mediator';

class ContentFieldMediator extends BaseMediator {

	constructor(viewComponent) {

		// super
		super(viewComponent);
	}

	onRegister() {}

	listNotificationInterests() {
		console.log(this + '.listNotificationInterests()');
		return [
			'TEST'
		];
	}

	handleNotification(note) {

		console.log(this + '.handleNotification()', note);
	}
}

ContentFieldMediator.prototype.NAME = 'ContentFieldMediator';

export default ContentFieldMediator;