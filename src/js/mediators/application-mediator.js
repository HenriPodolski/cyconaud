import BaseMediator from 'js/libs/base-mediator';

class ApplicationMediator extends BaseMediator {

	constructor(viewComponent) {

		// super
		super(viewComponent);
	}

	// onRegister() {}

	listNotificationInterests() {
		
		return [
			'TEST'
		];
	}

	handleNotification(note) {

		console.log(this + '.handleNotification()', note);
	}
}

ApplicationMediator.prototype.NAME = 'ApplicationMediator';

export default ApplicationMediator;