import BaseModel from 'js/libs/base-model';
import PouchDB from 'pouchdb';
import BackbonePouch from 'js/vendor/backbone-pouch';
import AppConsts from 'shared/app-consts';

class ContentModel extends BaseModel {

}

// PouchDB's default id attribute is _id
ContentModel.prototype.idAttribute = '_id';
ContentModel.prototype.defaults = {

	type: 'content',

	open: false
};
ContentModel.prototype.sync = BackbonePouch.sync({
	
	db: PouchDB(AppConsts.DATABASE)
});

export default ContentModel;