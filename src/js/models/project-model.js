import BaseModel from 'js/libs/base-model';
import PouchDB from 'pouchdb';
import BackbonePouch from 'js/vendor/backbone-pouch';
import AppConsts from 'shared/app-consts';

class ProjectModel extends BaseModel {

}

// PouchDB's default id attribute is _id
ProjectModel.prototype.idAttribute = '_id';
ProjectModel.prototype.defaults = {

	type: 'project'
};
ProjectModel.prototype.sync = BackbonePouch.sync({
	
	db: PouchDB(AppConsts.DATABASE)
});

export default ProjectModel;