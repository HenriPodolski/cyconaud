'use strict';

import BaseWebComponent from 'js/libs/base-webcomponent';
import SidebarView from 'js/views/sidebar-view';

class SidebarWebComponent extends BaseWebComponent {
	
	constructor(options) {

		this.setup = options || {};
		
		this.setup.tagName = 'ui-sidebar';

		this.setup.View = SidebarView;

		BaseWebComponent.prototype.constructor.call(this, this.setup);
	}
}

export default SidebarWebComponent;