'use strict';

import BaseWebComponent from 'js/libs/base-webcomponent';
import WorkspaceView from 'js/views/workspace-view';

class WorkspaceWebComponent extends BaseWebComponent {
	
	constructor(options) {

		this.setup = options || {};
		
		this.setup.tagName = 'ui-workspace';

		this.setup.View = WorkspaceView;

		BaseWebComponent.prototype.constructor.call(this, this.setup);
	}
}

export default WorkspaceWebComponent;