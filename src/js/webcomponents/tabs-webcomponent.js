'use strict';

import BaseWebComponent from 'js/libs/base-webcomponent';
import TabsView from 'js/views/tabs-view';

class TabsWebComponent extends BaseWebComponent {
	
	constructor(options) {

		this.setup = options || {};
		
		this.setup.tagName = 'ui-tabs';

		this.setup.View = TabsView;

		BaseWebComponent.prototype.constructor.call(this, this.setup);
	}
}

export default TabsWebComponent;