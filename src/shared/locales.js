export default {
	en: {
		a: 'a',
		add: 'add',
		array: 'list',
		content: 'content',
		field: 'field',
		hash: 'group',
		or: 'or',
		new: 'new',

		menu: {
			file: 'File',
			view: 'View',
			help: 'Help',
			save: 'Save',
			controlS: 'Ctrl + S'
		}				
	},

	de: {

	}
}