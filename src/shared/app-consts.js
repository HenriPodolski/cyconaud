export default {

	STARTUP: 'startup',

	DATABASE: 'cyconaud-db',

	PROJECTS_LOADED: 'projects:loaded',

	CONTENTS_LOADED: 'contents:loaded',

	SIDEBAR_READY: 'sidebar:ready',

	WORKSPACE_READY: 'workspace:ready',

	CONTENT_TYPE_ADD: 'contenttype:add',

	CONTENT_FIELD_ADD: 'contentfield:add',

	CONTENT_FIELD_CREATE: 'contentfield:create',

	CONTENT_GROUP_ADD: 'contentgroup:add',

	CONTENT_GROUP_CREATE: 'contentgroup:create',

	CONTENT_LIST_ADD: 'contentlist:add',

	CONTENT_LIST_CREATE: 'contentlist:create'
}